function showData(data) {

    $("#player-cards").html("Player cards: " + (data.player.playerHand).map(getCardString));
    $("#dealer-cards").html("Dealer cards: " + (data.dealer.dealerHand).map(getCardString));
    $("#dealer-score").html("Dealer score: " + data.dealer.dealerScore);
    $("#player-score").html("Player score: " + data.player.playerScore);
    $("#dealer-money").html("Dealer money: " + data.dealer.dealerMoney);
    $("#player-money").html("Player money: " + data.player.playerMoney);
}

function getCardString(hand) {
    return `${hand.Value } of ${hand.Suite}`;
}

function hide() {
    document.getElementById("hit-button").style.display = "none";
    document.getElementById("stand-button").style.display = "none";
}

function checkIfWin(data) {
    if (data.status.gameOver && data.status.playerWon) {
        alert("Player Won!!!");
        hide();
    } else if (data.status.gameOver && data.status.dealerWon) {
        alert("Dealer Won!!!");
        hide();
    } else if (data.status.gameOver){
        alert("It's a DRAW!!!");
        hide();
    }
    checkMoney(data);
}

function checkMoney(data) {
    if (data.player.playerMoney == 0 || data.dealer.dealerMoney == 0) {
        document.getElementById("new-game-button").style.display = "none";
        document.getElementById("hit-button").style.display = "none";
        document.getElementById("stand-button").style.display = "none";
    }
}

$(document).ready(function(){
    $('#new-game-button').click(function(){
        document.getElementById("hit-button").style.display = "inline";
        document.getElementById("stand-button").style.display = "inline";

        $.ajax({
            url: "/new",
            type: "POST",
            dataType: "json",
            success: function(data){
                showData(data);
            }
        });
    });
});

$(document).ready(function(){
    $('#hit-button').click(function(){

        $.ajax({
            url: "/hit",
            type: "POST",
            dataType: "json",
            success: function(data){
                showData(data);
                checkIfWin(data);
            }
        });
    });
});

$(document).ready(function(){
    $('#stand-button').click(function(){

        $.ajax({
            url: "/stand",
            type: "POST",
            dataType: "json",
            success: function(data){
                showData(data);
                checkIfWin(data);
            }
        });
    });
});