const Player = require("../routes/blackjack.js");
const blackjack = require("../routes/blackjack");
const chai = require("chai");
const assert = require("chai").assert;
//const getCardScore = require("../routes/blackjack").getCardScore;
const sinon = require("sinon");
const expect = chai.expect;

const app = require("../app");
const request = require("supertest");

//let player = new Player("Player", [{ Value: "5", Suite: "Hearts", Score: 5 }], 0, 100);

describe.skip("Test blackjack", function () {
    beforeEach(function () {
        sinon.restore();
    })
    it("Test the getCardScore method", function () {
        let result = blackjack.getCardScore({ Value: "2", Suite: "Hearts", Score: 2 });
        assert.equal(result, 2);
    });
    it("spy the reset method", function () {
        let spy = sinon.spy(player, "reset");
        console.log(player);
        player.reset();
        expect(spy.calledOnce).to.be.true;
    })
});

describe("api tests", () => {
    it("returns status code 200", async () => {
        const res = await request(app).post("/new");
        expect(res.statusCode).equal(200);
    });
    it("should update gameState", async() => {
        const res = await request(app).post("/new");
        //let spy = sinon.spy(blackjack, "shuffle");
        //expect(spy.calledOnce).to.be.true;
        console.log(res.body);
        expect(res.body.status.gameStarted).to.be.true;
    });
    it("dealer should have 1 card in hand", async() => {
        const res = await request(app).post("/new");
        expect(res.body.dealer.dealerHand.length).to.be.equal(1);
    })
});