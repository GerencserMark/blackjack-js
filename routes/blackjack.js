const express = require("express");
const router = express.Router();

const suites = ["Spades", "Hearts", "Diamonds", "Clubs"];
const values = [
    { label: "2", value: 2 }, { label: "3", value: 3 }, { label: "4", value: 4 },
    { label: "5", value: 5 }, { label: "6", value: 6 }, { label: "7", value: 7 },
    { label: "8", value: 8 }, { label: "9", value: 9 }, { label: "10", value: 10 },
    { label: "J", value: 10 }, { label: "Q", value: 10 }, { label: "K", value: 10 },
    { label: "A", value: 11 }
];

let deck = [];
let gameOver = false;
let gameStarted = false;
let playerWon = false;
let dealerWon = false;
let playerScore = 0;
let dealerScore = 0;
let playerMoney = 100;
let dealerMoney = 100;
let playerHand = [];
let dealerHand = [];
let card;

class Player {
    constructor(name, hand, score, money) {
        this.name = name;
        this.hand = hand;
        this.score = score;
        this.money = money;
    }
    reset() {
        this.hand = [];
        this.score = 0;
    }
    drawCard() {
        card = deck.pop();
        this.hand.push(card);
        return this.hand;
    }
    countScore() {
        this.score = 0;
        for (let i = 0; i < (this.hand).length; i++) {
            if (this.score >= 11 && (this.hand)[i].Value == 'A') { this.score += 1 }
            else {
                this.score += getCardScore((this.hand)[i]);
            }
        }
        return this.score;
    }
}
let dealer = new Player("Dealer", [], 0, 100);
let player = new Player("Player", [], 0, 100);

function createDeck() {
    deck = [];
    let score = 0;

    for (const value of values) {
        for (const suite of suites) {
            score = parseInt(value.value);
            let card = { Value: value.value, Suite: suite, Score: score };
            deck.push(card);
        }
    }
}

function shuffle(deck) {
    for (let i = deck.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * i)
        let k = deck[i]
        deck[i] = deck[j]
        deck[j] = k
    }
}

function checkScore() {

    if (playerScore == 21 || dealerScore > 21) {
        dealerMoney -= 10;
        playerMoney += 10;
        gameOver = true;
        playerWon = true;
        dealerWon = false;

    } else if (dealerScore == 21 || playerScore > 21) {
        playerMoney -= 10;
        dealerMoney += 10;
        gameOver = true;
        playerWon = false;
        dealerWon = true;

    } else if (dealerScore == playerScore && dealerScore >= 17) {
        gameOver = true;

    } else if (dealerScore >= 17 && dealerScore > playerScore) {
        playerMoney -= 10;
        dealerMoney += 10;
        gameOver = true;
        playerWon = false;
        dealerWon = true;
    } else if (dealerScore >= 17 && playerScore > dealerScore) {
        dealerMoney -= 10;
        playerMoney += 10;
        gameOver = true;
        playerWon = true;
        dealerWon = false;
    }
}

function getCardScore(card) {
    return card.Score;
}

function gameStateData() {
    let player = { playerHand, playerScore, playerMoney };
    let dealer = { dealerHand, dealerScore, dealerMoney };
    let status = { gameOver, gameStarted, playerWon, dealerWon }
    let gameState = { player, dealer, status };

    return gameState;
}

router.post('/new', (req, res) => {
    player.reset();
    dealer.reset();
    gameOver = false;
    gameStarted = true;
    playerWon = false;
    dealerWon = false;

    createDeck();
    shuffle(deck);
    dealerHand = dealer.drawCard();
    playerScore = player.countScore();
    dealerScore = dealer.countScore();
    res.send(gameStateData());
})

router.post('/hit', (req, res) => {
    playerHand = player.drawCard();
    playerScore = player.countScore();
    checkScore();
    res.send(gameStateData());
})

router.post('/stand', (req, res) => {
    while (dealerScore < 17) {
        dealerHand = dealer.drawCard();
        dealerScore = dealer.countScore();
    }
    dealerScore = dealer.countScore();
    checkScore();
    res.send(gameStateData());
})

module.exports = router;

// kikommentelni futtatáskor !!!
/*module.exports = {
    getCardScore: getCardScore,
    Player: Player
};*/

//module.exports.getCardScore = getCardScore;
//module.exports = Player;