const express = require("express");
const path = require("path");
const playerRouter = require("./routes/blackjack");
const app = express();

app.set("view engine", "ejs");
app.use(express.static("public"));

app.get("/", (req, res) => {
    console.log("Here");
    res.sendFile(path.join(__dirname, '/public/index.html'));
});

app.use(playerRouter);

module.exports = app;